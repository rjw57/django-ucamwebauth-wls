University of Cambridge Web Auth WLS
===============================================================================

Installation
````````````

Add the ``ucamwebauth_wls`` application to your
``INSTALLED_APPS`` configuration as usual.

Views and serializers
`````````````````````

.. automodule:: ucamwebauth_wls.views
    :members:

Default URL routing
```````````````````

.. automodule:: ucamwebauth_wls.urls
    :members:

Application configuration
`````````````````````````

.. automodule:: ucamwebauth_wls.apps
    :members:
