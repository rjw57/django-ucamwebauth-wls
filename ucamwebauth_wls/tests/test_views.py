import json
from urllib.parse import urlencode

from django.test import TestCase, RequestFactory
from django.urls import reverse

from .. import views


class ExampleTests(TestCase):
    def test_get(self):
        r = self.client.get(reverse('ucamwebauth_wls:example'))
        self.assertEqual(r.status_code, 200)


class AuthenticateTests(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.view = views.WLSView.as_view()

    def test_disallowed_methods(self):
        """Methods other than GET, HEAD and OPTIONS are not allowed."""
        for method in 'post put delete trace'.split():
            request = getattr(self.factory, method)('/')
            response = self.view(request)
            self.assertEqual(response.status_code, 405)  # method not allowed

    def test_allowed_methods(self):
        """GET, HEAD and OPTIONS methods do not returns a 405 Method Not Allowed status."""
        for method in 'get head options'.split():
            request = getattr(self.factory, method)('/')
            response = self.view(request)
            self.assertNotEqual(response.status_code, 405)  # method not allowed

    def test_unknown_parameter(self):
        """Unknown parameters returns a bad request response."""
        response = self.view(self.factory.get('/?some_odd_param=hello'))
        self._assert_400_response(response, 'UNKNOWN_PARAMETER', message_contains='some_odd_param')

    def test_missing_ver(self):
        """A missing ver parameter returns a bad request response."""
        response = self.view(self.factory.get('/?url=https://example.com/foo'))
        self._assert_400_response(response, 'MISSING_PARAMETER', message_contains='ver')

    def test_missing_url(self):
        """A missing url parameter returns a bad request response."""
        response = self.view(self.factory.get('/?ver=3'))
        self._assert_400_response(response, 'MISSING_PARAMETER', message_contains='url')

    def test_duplicate_parameter(self):
        """A duplicate parameter returns a bad request response."""
        response = self.view(self.factory.get('/?ver=3&url=https://example.com/foo&ver=2'))
        self._assert_400_response(response, 'DUPLICATE_PARAMETER', message_contains='ver')
        response = self.view(self.factory.get(
            '/?ver=3&url=https://example.com/foo&url=https://example.com/bar'))
        self._assert_400_response(response, 'DUPLICATE_PARAMETER', message_contains='url')

    def test_unsupported_version(self):
        """An unsupported protocol version returns a bad request response."""
        response = self.view(self.factory.get('/?ver=xxx&url=https://example.com/foo'))
        self._assert_400_response(response, 'UNSUPPORTED_PROTOCOL', message_contains='xxx')

    def test_relative_url(self):
        """A relative redirect URL returns a bad request response."""
        response = self.view(self.factory.get('/?ver=3&url=/foo'))
        self._assert_400_response(response, 'BAD_REDIRECT_URL', message_contains='/foo')

    def test_unsupported_scheme(self):
        """A redirect URL with unsupported scheme returns a bad request response."""
        response = self.view(self.factory.get('/?ver=3&url=ftp://example.com/foo'))
        self._assert_400_response(
            response, 'BAD_REDIRECT_URL', message_contains='ftp://example.com/foo')

    def test_bad_description(self):
        """
        A description with un-printable or non-ASCII characters returns a bad request
        response.

        """
        response = self.view(self.factory.get(
            '/?ver=3&url=http://example.com/foo&' + urlencode({'desc': 'with\nnewline'})
        ))
        self._assert_400_response(response, 'BAD_DESCRIPTION')
        response = self.view(self.factory.get(
            '/?ver=3&url=http://example.com/foo&' + urlencode({'desc': 'þø→ðæ'})
        ))
        self._assert_400_response(response, 'BAD_DESCRIPTION')

    def test_bad_message(self):
        """
        A message with un-printable or non-ASCII characters returns a bad request
        response.

        """
        response = self.view(self.factory.get(
            '/?ver=3&url=http://example.com/foo&' + urlencode({'msg': 'with\nnewline'})
        ))
        self._assert_400_response(response, 'BAD_MESSAGE')
        response = self.view(self.factory.get(
            '/?ver=3&url=http://example.com/foo&' + urlencode({'msg': 'þø→ðæ'})
        ))
        self._assert_400_response(response, 'BAD_MESSAGE')

    def _assert_400_response(self, response, expected_code, *, message_contains=None):
        self.assertEqual(response.status_code, 400)  # bad request
        self.assertEqual(response['Content-Type'], 'application/json')
        content = json.loads(response.content)
        self.assertEqual(content['error']['code'], expected_code)
        if message_contains is not None:
            self.assertIn(message_contains, content['error']['message'])
