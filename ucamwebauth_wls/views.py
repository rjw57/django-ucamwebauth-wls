"""
Views for University of Cambridge Web Auth WLS

"""
import dataclasses
import datetime
import typing
from urllib.parse import urlparse

from django.http import HttpResponse, HttpResponseBadRequest, JsonResponse
from django.views import View


# Create your views here.
def example(request):
    """
    A simple example view which renders the current date and time.

    """
    now = datetime.datetime.now()
    html = "<html><body>It is now %s.</body></html>" % now
    return HttpResponse(html)


@dataclasses.dataclass
class AuthenticationRequest:
    version: str
    url: str
    description: typing.Optional[str] = None
    acceptable_auth_types: typing.Tuple[str] = ('pwd',)
    require_reauth: bool = False
    non_interactive: bool = False
    message: typing.Optional[str] = None
    params: typing.Optional[str] = None
    redirect_on_failure: bool = True


class WLSError(Exception):
    error_code = 'UNKNOWN'

    def __init__(self, message):
        super().__init__(message)
        self.message = message


class UnknownParameterError(WLSError):
    error_code = 'UNKNOWN_PARAMETER'

    def __init__(self, parameter):
        super().__init__(f'Unknown parameter: {parameter}')


class DuplicateParameterError(WLSError):
    error_code = 'DUPLICATE_PARAMETER'

    def __init__(self, parameter):
        super().__init__(f'Duplicate parameter: {parameter}')


class MissingParameterError(WLSError):
    error_code = 'MISSING_PARAMETER'

    def __init__(self, parameter):
        super().__init__(f'Required parameter "{parameter}" not present')


class UnsupportedProtocolVersion(WLSError):
    error_code = 'UNSUPPORTED_PROTOCOL'

    def __init__(self, version):
        super().__init__(f'Unsupported protocol version: {version}')


class BadRedirectURL(WLSError):
    error_code = 'BAD_REDIRECT_URL'

    def __init__(self, url):
        super().__init__(f'Unsupported redirect URL: {url}')


class BadDescription(WLSError):
    error_code = 'BAD_DESCRIPTION'

    def __init__(self, desc):
        super().__init__(f'Description contains invalid characters: {desc!r}')


class BadMessage(WLSError):
    error_code = 'BAD_MESSAGE'

    def __init__(self, msg):
        super().__init__(f'Message contains invalid characters: {msg!r}')


#: A set of parameters which MAY be present in a WLS authentication request.
ALLOWED_AUTH_PARAMETERS = set('ver url desc aauth iact msg params date skew fail'.split())

#: A set of parameters which MUST be present in a WLS authentication request.
REQUIRED_AUTH_PARAMETERS = set('ver url'.split())

#: The protocol versions we support.
SUPPORTED_VERSIONS = {'1', '2', '3'}


class WLSView(View):
    http_method_names = ['get', 'head', 'options']

    def get(self, request, *args, **kwargs):
        try:
            auth_request = parse_and_validate_request(request)
        except WLSError as e:
            return JsonResponse(
                {'error': {'code': e.error_code, 'message': e.message}}, status=400)

        print(auth_request)

        return HttpResponseBadRequest('bad request')


def parse_and_validate_request(request):
    """
    Validates a WLS authentication request in the following manner:

    - Each required parameter is present.
    - No unrecognised parameters are present.
    - Each parameter appears only once.
    - The protocol version is supported.
    - The URL has a HTTP or HTTPS scheme and non-empty network location.
    - The desc parameter, if present, consists only of printable ASCII characters.
    - The msg parameter, if present, consists only of printable ASCII characters.

    If validation fails, an instance of WLSError is raised describing the error.

    The parsed request is returned as an AuthenticationRequest object.

    """
    for parameter, value in request.GET.lists():
        if parameter not in ALLOWED_AUTH_PARAMETERS:
            raise UnknownParameterError(parameter)
        if len(value) != 1:
            raise DuplicateParameterError(parameter)

    for required_parameter in REQUIRED_AUTH_PARAMETERS:
        if required_parameter not in request.GET:
            raise MissingParameterError(required_parameter)

    if request.GET['ver'] not in SUPPORTED_VERSIONS:
        raise UnsupportedProtocolVersion(request.GET['ver'])

    parsed_url = urlparse(request.GET['url'])
    if parsed_url.scheme not in {'http', 'https'} or parsed_url.netloc == '':
        raise BadRedirectURL(request.GET['url'])

    auth_request = AuthenticationRequest(version=request.GET['ver'], url=request.GET['url'])

    if 'desc' in request.GET:
        if not request.GET['desc'].isprintable() or not request.GET['desc'].isascii():
            raise BadDescription(request.GET['desc'])
        auth_request.description = request.GET['desc']

    if 'msg' in request.GET:
        if not request.GET['msg'].isprintable() or not request.GET['msg'].isascii():
            raise BadMessage(request.GET['msg'])
        auth_request.message = request.GET['msg']

    if 'aauth' in request.GET:
        auth_request.acceptable_auth_types = request.GET['aauth'].split(',')

    if request.GET.get('iact') == 'yes':
        auth_request.require_reauth = True

    if request.GET.get('iact') == 'no':
        auth_request.non_interactive = True

    if 'params' in request.GET:
        auth_request.params = request.GET['params']

    if request.GET.get('fail') == 'yes':
        auth_request.redirect_on_failure = False

    return auth_request
