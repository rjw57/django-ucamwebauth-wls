"""
URL routing schema for University of Cambridge Web Auth WLS.

"""

from django.urls import path

from . import views

app_name = "ucamwebauth_wls"

urlpatterns = [
    path('example', views.example, name='example'),
]
